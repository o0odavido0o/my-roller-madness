using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class MenuManager : MonoBehaviour {
	
	public static GameManager gm;
	
	[Tooltip("If not set, the player will default to the gameObject tagged as Player.")]
	public GameObject player;
	
	public enum gameStates {Playing, Death, GameOver, BeatLevel};
	public gameStates gameState = gameStates.Playing;
	
	public int score=0;
	public bool canBeatLevel = false;
	public int beatLevelScore=0;
	
	public GameObject mainCanvas;
	public Text mainScoreDisplay;
	public GameObject gameOverCanvas;
	public Text gameOverScoreDisplay;
	
	[Tooltip("Only need to set if canBeatLevel is set to true.")]
	public GameObject beatLevelCanvas;
	
	public AudioSource backgroundMusic;
	public AudioClip gameOverSFX;
	
	[Tooltip("Only need to set if canBeatLevel is set to true.")]
	public AudioClip beatLevelSFX;
	
	private Health playerHealth;
	
	void Start () {
	}
	
	void Update () {
	}

	public void Collect(int amount) {
	}
}
